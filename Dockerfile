FROM python:3.10-slim-bullseye

WORKDIR /app

COPY main.py .
COPY transaction.py .
COPY *.csv .

ENTRYPOINT [ "python3", "main.py" ]
CMD [ "5000" ]
