# Fetch Rewards Backend Engineering Take Home

> Have a great week, and thank you for your time! ~ Louis

### Setup
- Clone the repository (`git clone https://gitlab.com/LouisAsanaka/fetch-take-home-backend.git`).
- Enter the project directory (`cd fetch-take-home-backend`).
- **There are now two options**. The first option is recommended, and the second is just a convenient fallback.
    1. Local Python 3
        - Ensure a modern version of Python (3.6 or above) is installed.
        - Run the program (`python3 main.py 5000`).
    2. Docker
        - Ensure [Docker](https://docs.docker.com/get-docker) is installed.
        - Build the Docker image (`docker build -t fetch-point-transactions .`).
        - Run the project with the Docker image (`docker run -it fetch-point-transactions 5000`).

### Design
- I chose to write a small Python script using no third-party libraries. CSV parsing is handled with the `csv` standard library module.
- The script takes in one positional parameter, the number of points to spend per the given document. It also takes an optional parameter, the file to read from, as a quick way to test different types of transactions.
- The script performs checks against the list of transactions to ensure their validity. For example, the input argument should not be larger than the cumulative balance of the user. Also, transactions with negative points cannot take away more than the payer's balance at that point in time, regardless of the balance after future transactions.
- The script ensures that at any point in time, the balance of any payer remains >= 0 points.

### Assumptions
- The timestamp given is in a valid ISO-formatted form (without a timezone).
- The desired output can be unordered as long as it is valid JSON.
- The payer names are case-sensitive (script does no normalization).
- Terminating the script on an invalid input is desired behavior, as to not falsely set payer balances without a valid sequence of transactions.
