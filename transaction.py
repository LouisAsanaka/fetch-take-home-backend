from datetime import datetime


class Transaction:
    """Simple model class representing a Transaction.
    """

    __slots__ = ('payer', 'points', 'timestamp')
    
    def __init__(self, payer: str, points: str, timestamp_str: str):
        self.payer: str = payer
        try:
            self.points: int = int(points)
        except ValueError:
            raise RuntimeWarning(f'Invalid points "{points}"')
        try:
            # Assume timestamp is in ISO format without timezone
            self.timestamp: datetime = datetime.strptime(timestamp_str, '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            raise RuntimeWarning(f'Invalid timestamp "{timestamp_str}"')
    
    def __repr__(self):
        return f"Transaction(payer={self.payer}, points={self.points}, timestamp={self.timestamp})"
