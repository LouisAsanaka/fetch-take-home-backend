from argparse import ArgumentParser
from csv import DictReader as CSVDictReader
import json
from typing import List, Dict
import logging
logging.basicConfig(level=logging.WARNING)

from transaction import Transaction


TRANSACTION_FILE: str = './transactions.csv'


def parse_transactions(transactions_file: str = TRANSACTION_FILE) -> List[Transaction]:
    """Parses transactions from the file and sorts in ascending timestamp order.

    Args:
        transactions_file: the CSV file containing the transactions.
    Returns:
        a list of transactions, sorted in ascending timestamp order.

    """
    with open(transactions_file, 'r') as f:
        transactions: List[Transaction] = []
        reader = CSVDictReader(f, delimiter=',', quotechar='"')
        # Creates a Transaction model for every row of the CSV file
        for row in reader:
            transactions.append(Transaction(
                payer=row['payer'], points=row['points'], 
                timestamp_str=row['timestamp']))
        # Sort the transactions in ascending timestamp order
        transactions.sort(key=lambda t: t.timestamp)
        return transactions

def main():
    """Main function that computes partner (payer) point balances given a
    sequence of transactions from a CSV file.
    """
    # Parse command-line arguments
    parser = ArgumentParser()
    parser.add_argument('points', help='number the points to spend', type=int)
    parser.add_argument('--file', help='.csv file to load transactions from', 
        default=TRANSACTION_FILE)
    args = parser.parse_args()

    points_remaining = args.points  # points that need to be spent
    transactions = parse_transactions(args.file)

    total_payers_balance: int = 0
    payer_balance: Dict[str, int] = {}
    # Compute current payer balances while validating the transactions
    for transaction in transactions:
        if transaction.payer not in payer_balance:
            payer_balance[transaction.payer] = 0
        # Terminate if a spending transaction (negative points) spends
        # more points than the payer has at that point in time
        if transaction.points < 0 and payer_balance[transaction.payer] + transaction.points < 0:
            raise RuntimeError(f'Invalid transaction {transaction} with '
                f'current payer balance of {payer_balance[transaction.payer]}')
        # Accumulate payer's balance
        payer_balance[transaction.payer] += transaction.points
        total_payers_balance += transaction.points
    
    # Terminate if the user tries to spend more than the total balance
    if points_remaining > total_payers_balance:
        raise RuntimeError(f'Tried to spend {points_remaining} points when '
            f'balance is {total_payers_balance} points')
    
    logging.debug(f'Initial balances: {payer_balance}')

    # Start allocating spent points to payers by oldest points
    for transaction in transactions:
        # Exit when all spent points have been allocated to payers
        if points_remaining == 0:
            break
        # Subtract only as much as the transaction has without exceeding the
        # payer's balance
        amount_to_subtract = min(points_remaining, transaction.points, 
            payer_balance[transaction.payer])
        logging.debug(
            f'-{amount_to_subtract} points from transaction {transaction}')
        # Subtract the payer's balance
        payer_balance[transaction.payer] -= amount_to_subtract
        # Subtract the spent points allocated in this transaction
        points_remaining -= amount_to_subtract
    
    logging.debug(f'Points still needed to be spent: {points_remaining}')

    # Print the final payer balances in JSON
    print(json.dumps(payer_balance, indent=4))


if __name__ == '__main__':
    main()
